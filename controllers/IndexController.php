<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use core\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $this->loadTemplate('index/index');
    }

    public function upload()
    {
        if(!empty($_POST)){

            if (!isset($_FILES['foto']) || $_FILES['foto']['error'] == 4){
                echo 'Nenhum arquivo enviado';
                exit;
            }

            $arquivo = $_FILES['foto'];
            move_uploaded_file($arquivo['tmp_name'], FULL_PATH . 'assets/images/' . $arquivo['name']);

            echo 'Imagem enviada com sucesso';
            exit;

        }
    }

}
