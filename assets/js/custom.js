// Para enviar de um formulário

// $('#form').on('submit', function (e) {
//     e.preventDefault();
//
//     var form = $('#form')[0];
//     var data = new FormData(form);
//
//     $.ajax({
//         type:'POST',
//         url: '/index/upload/',
//         data: data,
//         contentType: false,
//         processData: false,
//         success: function (msg) {
//             alert(msg);
//         }
//     });
// });

// Para enviar de um botão qualuqer

$('button').on('click', function () {
    var data = new FormData();

    var arquivos = $('#foto')[0].files;

    if (arquivos.length > 0){

        data.append('nome', $('#nome').val());
        data.append('foto', arquivos[0]);

        $.ajax({
            type:'POST',
            url: '/index/upload/',
            data: data,
            contentType: false,
            processData: false,
            success: function (msg) {
                alert(msg);
            }
        });

    }

})